from flask import Flask, render_template, jsonify,redirect, session,url_for
from functools import wraps
import json
from os import environ as env
from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv, find_dotenv
from authlib.flask.client import OAuth
from six.moves.urllib.parse import urlencode
import constants

ENV_FILE = find_dotenv()
if ENV_FILE:
    load_dotenv(ENV_FILE)

AUTH0_CALLBACK_URL = env.get(constants.AUTH0_CALLBACK_URL)
AUTH0_CLIENT_ID = env.get(constants.AUTH0_CLIENT_ID)
AUTH0_CLIENT_SECRET = env.get(constants.AUTH0_CLIENT_SECRET)
AUTH0_DOMAIN = env.get(constants.AUTH0_DOMAIN)
AUTH0_BASE_URL = 'https://' + AUTH0_DOMAIN
AUTH0_AUDIENCE = env.get(constants.AUTH0_AUDIENCE)
if AUTH0_AUDIENCE is '':
    AUTH0_AUDIENCE = AUTH0_BASE_URL + '/userinfo'

app = Flask(__name__, static_url_path='/static', static_folder='./static')
app.secret_key = constants.SECRET_KEY
app.debug = True


@app.errorhandler(Exception)
def handle_auth_error(ex):
    response = jsonify(message=str(ex))
    response.status_code = (ex.code if isinstance(ex, HTTPException) else 500)
    return response


oauth = OAuth(app)

auth0 = oauth.register(
    'auth0',
    client_id='nUqG1ZVygeN9U7dO3Oy1tVUlBkzF5xeu',
    client_secret='gU2m-COqqwBUwo6Pgstwe_itlZMIc6iGaceWOz27fglgc2Q54V6bcRB5eY1CEl0b',
    api_base_url='https://dev-qpkfuqxh.auth0.com',
    access_token_url='https://dev-qpkfuqxh.auth0.com/oauth/token',
    authorize_url='https://dev-qpkfuqxh.auth0.com/authorize',
    client_kwargs={
        'scope': 'openid profile',
    },
)


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if constants.PROFILE_KEY not in session:
            return redirect('/log')
        return f(*args, **kwargs)

    return decorated

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/home')
def auth():
    return render_template('home.html')

@app.route('/callback')
def callback():
    auth0.authorize_access_token()
    resp = auth0.get('userinfo')
    userinfo = resp.json()

    session[constants.JWT_PAYLOAD] = userinfo
    session[constants.PROFILE_KEY] = {
        'user_id': userinfo['sub'],
        'name': userinfo['name'],
        'picture': userinfo['picture']
    }
    return render_template('index.html')


@app.route('/log')
def log():
    return auth0.authorize_redirect(redirect_uri='http://localhost:5000/callback', audience='https://dev-qpkfuqxh.auth0.com/userinfo')

@app.route('/login')
def login():
    return auth0.authorize_redirect(redirect_uri='http://localhost:5000/success_sso', audience='https://dev-qpkfuqxh.auth0.com/userinfo')
@app.route('/login_1')
def login_1():
    return auth0.authorize_redirect(redirect_uri='https://www.odisha.gov.in/', audience='https://dev-qpkfuqxh.auth0.com/userinfo')

@app.route('/login_2')
def bhulekha():
    return auth0.authorize_redirect(redirect_uri='http://bhulekh.ori.nic.in/rccms', audience='https://dev-qpkfuqxh.auth0.com/userinfo')

@app.route('/login_3')
def cmg():
    return auth0.authorize_redirect(redirect_uri='http://cmgcodisha.gov.in/', audience='https://dev-qpkfuqxh.auth0.com/userinfo')


@app.route('/logout')
def logout():
    session.clear()
    params = {'returnTo': url_for('home', _external=True), 'client_id': 'nUqG1ZVygeN9U7dO3Oy1tVUlBkzF5xeu'}
    return redirect(auth0.api_base_url + '/v2/logout?' + urlencode(params))

@app.route('/dashboard')
@requires_auth
def dashboard():
    return render_template('dashboard.html',
                           userinfo=session[constants.PROFILE_KEY],
                           userinfo_pretty=json.dumps(session[constants.JWT_PAYLOAD], indent=4))

@app.route('/edis')
def e_dispatch():
    return render_template('e_dispatch.html')

@app.route('/success_sso')
#@requires_auth
def test():
    auth0.authorize_access_token()
    resp = auth0.get('userinfo')
    userinfo = resp.json()

    session[constants.JWT_PAYLOAD] = userinfo
    session[constants.PROFILE_KEY] = {
        'user_id': userinfo['sub'],
        'name': userinfo['name'],
        'picture': userinfo['picture']
    }
    return render_template('e_dispatch.html', userinfo=session[constants.PROFILE_KEY], userinfo_pretty=json.dumps(session[constants.JWT_PAYLOAD], indent=4))


if __name__ == '__main__':

    app.run(debug=True)