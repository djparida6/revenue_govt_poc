/*!
 * ASP.NET SignalR JavaScript Library v1.1.4
 * http://signalr.net/
 *
 * Copyright Microsoft Open Technologies, Inc. All rights reserved.
 * Licensed under the Apache 2.0
 * https://github.com/SignalR/SignalR/blob/master/LICENSE.md
 *
 */

/// <reference path="..\..\SignalR.Client.JS\Scripts\jquery-1.6.4.js" />
/// <reference path="jquery.signalR.js" />
(function ($, window) {
    /// <param name="$" type="jQuery" />
    "use strict";

    if (typeof ($.signalR) !== "function") {
        throw new Error("SignalR: SignalR is not loaded. Please ensure jquery.signalR-x.js is referenced before ~/signalr/hubs.");
    }

    var signalR = $.signalR;

    function makeProxyCallback(hub, callback) {
        return function () {
            // Call the client hub method
            callback.apply(hub, $.makeArray(arguments));
        };
    }

    function registerHubProxies(instance, shouldSubscribe) {
        var key, hub, memberKey, memberValue, subscriptionMethod;

        for (key in instance) {
            if (instance.hasOwnProperty(key)) {
                hub = instance[key];

                if (!(hub.hubName)) {
                    // Not a client hub
                    continue;
                }

                if (shouldSubscribe) {
                    // We want to subscribe to the hub events
                    subscriptionMethod = hub.on;
                }
                else {
                    // We want to unsubscribe from the hub events
                    subscriptionMethod = hub.off;
                }

                // Loop through all members on the hub and find client hub functions to subscribe/unsubscribe
                for (memberKey in hub.client) {
                    if (hub.client.hasOwnProperty(memberKey)) {
                        memberValue = hub.client[memberKey];

                        if (!$.isFunction(memberValue)) {
                            // Not a client hub function
                            continue;
                        }

                        subscriptionMethod.call(hub, memberKey, makeProxyCallback(hub, memberValue));
                    }
                }
            }
        }
    }

    $.hubConnection.prototype.createHubProxies = function () {
        var proxies = {};
        this.starting(function () {
            // Register the hub proxies as subscribed
            // (instance, shouldSubscribe)
            registerHubProxies(proxies, true);

            this._registerSubscribedHubs();
        }).disconnected(function () {
            // Unsubscribe all hub proxies when we "disconnect".  This is to ensure that we do not re-add functional call backs.
            // (instance, shouldSubscribe)
            registerHubProxies(proxies, false);
        });

        proxies.citizenSender = this.createHubProxy('citizenSender'); 
        proxies.citizenSender.client = { };
        proxies.citizenSender.server = {
            citizenToClient: function (messages) {
            /// <summary>Calls the CitizenToClient method on the server-side CitizenSender hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"messages\" type=\"Object\">Server side type is System.Collections.Generic.IEnumerable`1[System.String]</param>
                return proxies.citizenSender.invoke.apply(proxies.citizenSender, $.merge(["CitizenToClient"], $.makeArray(arguments)));
             }
        };

        proxies.headerCount = this.createHubProxy('headerCount'); 
        proxies.headerCount.client = { };
        proxies.headerCount.server = {
            dispatchToClient: function (messages) {
            /// <summary>Calls the DispatchToClient method on the server-side HeaderCount hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"messages\" type=\"Object\">Server side type is System.Collections.Generic.IEnumerable`1[System.String]</param>
                return proxies.headerCount.invoke.apply(proxies.headerCount, $.merge(["DispatchToClient"], $.makeArray(arguments)));
             }
        };

        proxies.messageSender = this.createHubProxy('messageSender'); 
        proxies.messageSender.client = { };
        proxies.messageSender.server = {
            dispatchToClient: function (messages) {
            /// <summary>Calls the DispatchToClient method on the server-side MessageSender hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"messages\" type=\"Object\">Server side type is System.Collections.Generic.IEnumerable`1[System.String]</param>
                return proxies.messageSender.invoke.apply(proxies.messageSender, $.merge(["DispatchToClient"], $.makeArray(arguments)));
             },

            showjson: function () {
            /// <summary>Calls the Showjson method on the server-side MessageSender hub.&#10;Returns a jQuery.Deferred() promise.</summary>
                return proxies.messageSender.invoke.apply(proxies.messageSender, $.merge(["Showjson"], $.makeArray(arguments)));
             }
        };

        proxies.notify = this.createHubProxy('notify'); 
        proxies.notify.client = { };
        proxies.notify.server = {
            getData: function () {
            /// <summary>Calls the GetData method on the server-side Notify hub.&#10;Returns a jQuery.Deferred() promise.</summary>
                return proxies.notify.invoke.apply(proxies.notify, $.merge(["GetData"], $.makeArray(arguments)));
             },

            send: function (msg) {
            /// <summary>Calls the Send method on the server-side Notify hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"msg\" type=\"Object\">Server side type is System.Collections.ArrayList</param>
                return proxies.notify.invoke.apply(proxies.notify, $.merge(["Send"], $.makeArray(arguments)));
             },

            show: function () {
            /// <summary>Calls the Show method on the server-side Notify hub.&#10;Returns a jQuery.Deferred() promise.</summary>
                return proxies.notify.invoke.apply(proxies.notify, $.merge(["Show"], $.makeArray(arguments)));
             }
        };

        proxies.receiptSender = this.createHubProxy('receiptSender'); 
        proxies.receiptSender.client = { };
        proxies.receiptSender.server = {
            receiptToClient: function (messages) {
            /// <summary>Calls the ReceiptToClient method on the server-side ReceiptSender hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"messages\" type=\"Object\">Server side type is System.Collections.Generic.IEnumerable`1[System.String]</param>
                return proxies.receiptSender.invoke.apply(proxies.receiptSender, $.merge(["ReceiptToClient"], $.makeArray(arguments)));
             }
        };

        proxies.todayCitizenSender = this.createHubProxy('todayCitizenSender'); 
        proxies.todayCitizenSender.client = { };
        proxies.todayCitizenSender.server = {
            todayCitizenToClient: function (messages) {
            /// <summary>Calls the TodayCitizenToClient method on the server-side TodayCitizenSender hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"messages\" type=\"Object\">Server side type is System.Collections.Generic.IEnumerable`1[System.String]</param>
                return proxies.todayCitizenSender.invoke.apply(proxies.todayCitizenSender, $.merge(["TodayCitizenToClient"], $.makeArray(arguments)));
             }
        };

        proxies.todayMessageSender = this.createHubProxy('todayMessageSender'); 
        proxies.todayMessageSender.client = { };
        proxies.todayMessageSender.server = {
            showjson: function () {
            /// <summary>Calls the Showjson method on the server-side TodayMessageSender hub.&#10;Returns a jQuery.Deferred() promise.</summary>
                return proxies.todayMessageSender.invoke.apply(proxies.todayMessageSender, $.merge(["Showjson"], $.makeArray(arguments)));
             },

            todayDispatchToClient: function (messages) {
            /// <summary>Calls the TodayDispatchToClient method on the server-side TodayMessageSender hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"messages\" type=\"Object\">Server side type is System.Collections.Generic.IEnumerable`1[System.String]</param>
                return proxies.todayMessageSender.invoke.apply(proxies.todayMessageSender, $.merge(["TodayDispatchToClient"], $.makeArray(arguments)));
             }
        };

        proxies.todayReceiptSender = this.createHubProxy('todayReceiptSender'); 
        proxies.todayReceiptSender.client = { };
        proxies.todayReceiptSender.server = {
            todayReceiptToClient: function (messages) {
            /// <summary>Calls the TodayReceiptToClient method on the server-side TodayReceiptSender hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"messages\" type=\"Object\">Server side type is System.Collections.Generic.IEnumerable`1[System.String]</param>
                return proxies.todayReceiptSender.invoke.apply(proxies.todayReceiptSender, $.merge(["TodayReceiptToClient"], $.makeArray(arguments)));
             }
        };

        return proxies;
    };

    signalR.hub = $.hubConnection("https://push.edodisha.gov.in/signalr", { useDefaultPath: false });
    $.extend(signalR, signalR.hub.createHubProxies());

}(window.jQuery, window));