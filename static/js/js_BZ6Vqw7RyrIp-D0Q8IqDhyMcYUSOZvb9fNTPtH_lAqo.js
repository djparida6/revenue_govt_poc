// $Id: jquery.pagestyle.js,v 1.4 2009/12/24 16:49:09 christianzwahlen Exp $

(function($) {
  $(document).ready(function(){
    $("a.ps_white_black").attr({ href: "#" });
    $("a.ps_black_white").attr({ href: "#" });
    $("a.ps_yellow_blue").attr({ href: "#" });
    $("a.ps_standard").attr({ href: "#" });
    function PSremoveBC(){
      $("body").removeClass("pagestyle_black_white");
      $("body").removeClass("pagestyle_white_black");
      $("body").removeClass("pagestyle_yellow_blue");
      $("body").removeClass("pagestyle_standard");
      $("link.ps_white_black").attr({ rel: "alternate stylesheet" });
      $("link.ps_black_white").attr({ rel: "alternate stylesheet" });
      $("link.ps_yellow_blue").attr({ rel: "alternate stylesheet" });
      $("link.ps_standard").attr({ rel: "alternate stylesheet" });
    };
    function editLink(link_href){
      var ps_val = $("#edit-pagestyle-select").val();
      if (link_href = "standard") {
        $("link.ps_" + pagestyleCurrent).attr({ href: pagestylePath + "/css/style_standard.css" });
      }
      else if (link_href = "current") {
        $("link.ps_" + pagestyleCurrent).attr({ href: pagestylePath + "/css/style_" + pagestyleCurrent + ".css" });
      }
      $("link.ps_" + ps_val).attr({ href: pagestylePath + "/css/style_" + ps_val + ".css" });
    }
    $("a.ps_black_white[href=#]").click(
      function(){
        PSremoveBC();
        editLink(link_href = "current");
        $.cookie('pagestyle', "black_white", { expires: pagestyleCookieExpires, path: pagestyleCookieDomain});
        $("#pagestyle_current").empty();
        $("#pagestyle_current").append(Drupal.t('Black') + '/'+ Drupal.t('White'));
        $("#pagestyle_current").attr({ title: Drupal.t('Current Style') + ": " + Drupal.t('Black') + '/'+ Drupal.t('White')});
        $("body").addClass("pagestyle_black_white");
        return false;
      }
    );
    $("a.ps_white_black[href=#]").click(
    function (){
        PSremoveBC();
        editLink(link_href = "current");
        $.cookie('pagestyle', "white_black", { expires: pagestyleCookieExpires, path: pagestyleCookieDomain});
        $("#pagestyle_current").empty();
        $("#pagestyle_current").append(Drupal.t('White') + '/' + Drupal.t('Black'));
        $("#pagestyle_current").attr({ title: Drupal.t('Current Style') + ": " + Drupal.t('White') + '/'+ Drupal.t('Black')});
        $("body").addClass("pagestyle_white_black");
        return false;
      }
    );
    $("a.ps_yellow_blue[href=#]").click(
      function(){
        PSremoveBC();
        editLink(link_href = "current");
        $.cookie('pagestyle', "yellow_blue", { expires: pagestyleCookieExpires, path: pagestyleCookieDomain});
        $("#pagestyle_current").empty();
        $("#pagestyle_current").append(Drupal.t('Yellow') + '/' + Drupal.t('Blue'));
        $("#pagestyle_current").attr({ title: Drupal.t('Current Style') + ": " + Drupal.t('Yellow') + '/'+ Drupal.t('Blue')});
        $("body").addClass("pagestyle_yellow_blue");
        return false;
      }
    );
    $("a.ps_standard[href=#]").click(
      function(){
        PSremoveBC();
        editLink(link_href = "standard");
        $.cookie('pagestyle', "standard", { expires: pagestyleCookieExpires, path: pagestyleCookieDomain});
        $("#pagestyle_current").empty();
        $("#pagestyle_current").append(Drupal.t('Standard'));
        $("#pagestyle_current").attr({ title: Drupal.t('Current Style') + ": " + Drupal.t('Standard')});
        $("body").addClass("pagestyle_standard");
        return false;
      }
    );
    function pagestyleVals() {
      var ps_val = $("#edit-pagestyle-select").val();
        PSremoveBC();
        editLink(link_href = "standard");
        $.cookie('pagestyle', ps_val, { expires: pagestyleCookieExpires, path: pagestyleCookieDomain});
        $("body").addClass("pagestyle_" + ps_val);
        $("select.pagestyle option:selected").each(function () {
          $("#pagestyle_current").empty();
          $("#pagestyle_current").append( $(this).text() );
          }
        );
        $("body").addClass('pagestyle_' + ps_val);
        $("link.ps_" + ps_val).attr({ rel: "stylesheet" });
    }
    $("#edit-pagestyle-select").change(pagestyleVals);
    $("#edit-pagestyle-submit").hide();
    $("img.ps_rollover").hover(
      function(){
        if($(this).attr("src").indexOf("16_hover") == -1) {
          var newSrc = $(this).attr("src").replace("16.gif","16_hover.gif#hover");
          $(this).attr("src",newSrc);
        }
      },
      function(){
        if($(this).attr("src").indexOf("16_hover.gif#hover") != -1) {
          var oldSrc = $(this).attr("src").replace("16_hover.gif#hover","16.gif");
          $(this).attr("src",oldSrc);
        }
        else if($(this).attr("src").indexOf("16_focus.gif#focus") != -1) {
          var oldSrc = $(this).attr("src").replace("16_focus.gif#focus","16.gif");
          $(this).attr("src",oldSrc);
        }
      }
    );
    $("a.ps_rollover").focus(
      function(){
        var tsIMG = $(this).children("img");
        if($(tsIMG).attr("src").indexOf("16_hover.gif#hover") != -1) {
          var newSrc = $(tsIMG).attr("src").replace("16_hover.gif#hover","16_focus.gif#focus");
          $(tsIMG).attr("src",newSrc);
        }
      }
    );
    function pagestyleFW() {
      var ps_fw_bw = $("#edit-pagestyle-fontweight-black-white").val();
      var ps_fw_wb = $("#edit-pagestyle-fontweight-white-black").val();
      var ps_fw_yb = $("#edit-pagestyle-fontweight-yellow-blue").val();
      var ps_fw_s = $("#edit-pagestyle-fontweight-standard").val();
      $("div.form-item-pagestyle-fontweight-black-white label").css({"font-weight": ps_fw_bw});
      $("div.form-item-pagestyle-fontweight-white-black label").css({"font-weight": ps_fw_wb});
      $("div.form-item-pagestyle-fontweight-yellow-blue label").css({"font-weight": ps_fw_yb});
      $("div.form-item-pagestyle-fontweight-standard label").css({"font-weight": ps_fw_s});
    }
    $("#edit-pagestyle-fontweight-black-white").change(pagestyleFW);
    $("#edit-pagestyle-fontweight-white-black").change(pagestyleFW);
    $("#edit-pagestyle-fontweight-yellow-blue").change(pagestyleFW);
    $("#edit-pagestyle-fontweight-standard").change(pagestyleFW);
  });
})(jQuery);
;

/**
 * Cookie plugin 1.0
 *
 * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */
jQuery.cookie=function(b,j,m){if(typeof j!="undefined"){m=m||{};if(j===null){j="";m.expires=-1}var e="";if(m.expires&&(typeof m.expires=="number"||m.expires.toUTCString)){var f;if(typeof m.expires=="number"){f=new Date();f.setTime(f.getTime()+(m.expires*24*60*60*1000))}else{f=m.expires}e="; expires="+f.toUTCString()}var l=m.path?"; path="+(m.path):"";var g=m.domain?"; domain="+(m.domain):"";var a=m.secure?"; secure":"";document.cookie=[b,"=",encodeURIComponent(j),e,l,g,a].join("")}else{var d=null;if(document.cookie&&document.cookie!=""){var k=document.cookie.split(";");for(var h=0;h<k.length;h++){var c=jQuery.trim(k[h]);if(c.substring(0,b.length+1)==(b+"=")){d=decodeURIComponent(c.substring(b.length+1));break}}}return d}};
;
Drupal.TBMegaMenu = Drupal.TBMegaMenu || {};

(function ($) {
  Drupal.TBMegaMenu.oldWindowWidth = 0;
  Drupal.TBMegaMenu.displayedMenuMobile = false;
  Drupal.TBMegaMenu.supportedScreens = [980];
  Drupal.TBMegaMenu.menuResponsive = function () {
    var windowWidth = window.innerWidth ? window.innerWidth : $(window).width();
    var navCollapse = $('.tb-megamenu').children('.nav-collapse');
    if (windowWidth < Drupal.TBMegaMenu.supportedScreens[0]) {
      navCollapse.addClass('collapse');
      if (Drupal.TBMegaMenu.displayedMenuMobile) {
        navCollapse.css({height: 'auto', overflow: 'visible'});
      } else {
        navCollapse.css({height: 0, overflow: 'hidden'});
      }
    } else {
      // If width of window is greater than 980 (supported screen).
      navCollapse.removeClass('collapse');
      if (navCollapse.height() <= 0) {
        navCollapse.css({height: 'auto', overflow: 'visible'});
      }
    }
  };
  
  Drupal.behaviors.tbMegaMenuAction = {
    attach: function(context) {
      $('.tb-megamenu-button', context).once('menuIstance', function () {
        var This = this;
        $(This).click(function() {
          if(parseInt($(this).parent().children('.nav-collapse').height())) {
            $(this).parent().children('.nav-collapse').css({height: 0, overflow: 'hidden'});
            Drupal.TBMegaMenu.displayedMenuMobile = false;
          }
          else {
            $(this).parent().children('.nav-collapse').css({height: 'auto', overflow: 'visible'});
            Drupal.TBMegaMenu.displayedMenuMobile = true;
          }
        });
      });
      
      
      var isTouch = 'ontouchstart' in window && !(/hp-tablet/gi).test(navigator.appVersion);
      if(!isTouch){
        $(document).ready(function($){
          var mm_duration = 0;
          $('.tb-megamenu').each (function(){
            if ($(this).data('duration')) {
              mm_duration = $(this).data('duration');
            }
          });
          var mm_timeout = mm_duration ? 100 + mm_duration : 500;
          $('.nav > li, li.mega').hover(function(event) {
            var $this = $(this);
            if ($this.hasClass ('mega')) {
              $this.addClass ('animating');
              clearTimeout ($this.data('animatingTimeout'));
              $this.data('animatingTimeout', setTimeout(function(){$this.removeClass ('animating')}, mm_timeout));
              clearTimeout ($this.data('hoverTimeout'));
              $this.data('hoverTimeout', setTimeout(function(){$this.addClass ('open')}, 100));  
            } else {
              clearTimeout ($this.data('hoverTimeout'));
              $this.data('hoverTimeout', 
              setTimeout(function(){$this.addClass ('open')}, 100));
            }
          },
          function(event) {
            var $this = $(this);
            if ($this.hasClass ('mega')) {
              $this.addClass ('animating');
              clearTimeout ($this.data('animatingTimeout'));
              $this.data('animatingTimeout', 
              setTimeout(function(){$this.removeClass ('animating')}, mm_timeout));
              clearTimeout ($this.data('hoverTimeout'));
              $this.data('hoverTimeout', setTimeout(function(){$this.removeClass ('open')}, 100));
            } else {
              clearTimeout ($this.data('hoverTimeout'));
              $this.data('hoverTimeout', 
              setTimeout(function(){$this.removeClass ('open')}, 100));
            }
          });
        });
      }
      
      $(window).resize(function() {
        var windowWidth = window.innerWidth ? window.innerWidth : $(window).width();
        if(windowWidth != Drupal.TBMegaMenu.oldWindowWidth){
          Drupal.TBMegaMenu.oldWindowWidth = windowWidth;
          Drupal.TBMegaMenu.menuResponsive();
        }
      });
    },
  }
})(jQuery);

;
Drupal.TBMegaMenu = Drupal.TBMegaMenu || {};

(function ($) {
  Drupal.TBMegaMenu.createTouchMenu = function(items) {
      items.children('a').each( function() {
	var $item = $(this);
        var tbitem = $(this).parent();
        $item.click( function(event){
          if ($item.hasClass('tb-megamenu-clicked')) {
            var $uri = $item.attr('href');
            window.location.href = $uri;
          }
          else {
            event.preventDefault();
            $item.addClass('tb-megamenu-clicked');
            if(!tbitem.hasClass('open')){	
              tbitem.addClass('open');
            }
          }
        }).closest('li').mouseleave( function(){
          $item.removeClass('tb-megamenu-clicked');
          tbitem.removeClass('open');
        });
     });
     /*
     items.children('a').children('span.caret').each( function() {
	var $item = $(this).parent();
        $item.click(function(event){
          tbitem = $item.parent();
          if ($item.hasClass('tb-megamenu-clicked')) {
            Drupal.TBMegaMenu.eventStopPropagation(event);
            if(tbitem.hasClass('open')){	
              tbitem.removeClass('open');
              $item.removeClass('tb-megamenu-clicked');
            }
          }
          else {
            Drupal.TBMegaMenu.eventStopPropagation(event);
            $item.addClass('tb-megamenu-clicked');
            if(!tbitem.hasClass('open')){	
              tbitem.addClass('open');
              $item.removeClass('tb-megamenu-clicked');
            }
          }
        });
     });
     */
  }
  
  Drupal.TBMegaMenu.eventStopPropagation = function(event) {
    if (event.stopPropagation) {
      event.stopPropagation();
    }
    else if (window.event) {
      window.event.cancelBubble = true;
    }
  }  
  Drupal.behaviors.tbMegaMenuTouchAction = {
    attach: function(context) {
      var isTouch = 'ontouchstart' in window && !(/hp-tablet/gi).test(navigator.appVersion);
      if(isTouch){
        $('html').addClass('touch');
        Drupal.TBMegaMenu.createTouchMenu($('.tb-megamenu ul.nav li.mega').has('.dropdown-menu'));
      }
    }
  }
})(jQuery);
;
(function ($) {
  Drupal.behaviors.viewsBootstrapCarousel = {
    attach: function(context, settings) {
      $(function () {
        $.each(settings.viewsBootstrap.carousel, function(id, carousel) {
          try {
            $('#views-bootstrap-carousel-' + carousel.id, context).carousel(carousel.attributes);
          }
          catch(err) {
            console.log(err);
          }
        });
      });
    }
  };
})(jQuery);
;
(function ($) {

	$.fn.newsTickerFade = function (b) {
		b = b || 4000;
		initTicker = function (a) {
			stopTicker(a);
			a.items = $("li", a);
			a.items.not(":eq(0)").hide().end();
			a.currentitem = 0;
			startTicker(a)
		};
		startTicker = function (a) {
			a.tickfn = setInterval(function () {
				doTick(a)
			},
			b)
		};
		stopTicker = function (a) {
			clearInterval(a.tickfn)
		};
		pauseTicker = function (a) {
			a.pause = true
		};
		resumeTicker = function (a) {
			a.pause = false
		};
		doTick = function (a) {
			if (a.pause) return;
			a.pause = true;
			$(a.items[a.currentitem]).fadeOut("slow", function () {
				$(this).hide();
				a.currentitem = ++a.currentitem % (a.items.size());
				$(a.items[a.currentitem]).fadeIn("slow", function () {
					a.pause = false
				})
			})
		};
		this.each(function () {
			if (this.nodeName.toLowerCase() != "ul") return;
			initTicker(this)
		}).addClass("newsticker").hover(function () {
			pauseTicker(this)
		},
		function () {
			resumeTicker(this)
		});
		return this
	}

})(jQuery);
;