from flask import Flask,redirect,render_template,request,url_for,session, jsonify, make_response
from functools import wraps
from werkzeug import security
from werkzeug.exceptions import HTTPException
from authlib.flask.client import OAuth
from six.moves.urllib.parse import urlencode
import gc, os, json, constants, ssl
import urllib.request as logo


app = Flask(__name__, static_url_path='/static', static_folder='./static')
app.secret_key = constants.SECRET_KEY
app.debug = True

AUTH0_CALLBACK_URL = "http://casa.govt.cent/callback"
AUTH0_CLIENT_ID = "@!4A2A.866E.4B13.6C36!0001!FBF0.9C4B!0008!1FDD.37A1.2898.2FC4"
AUTH0_CLIENT_SECRET = "revenue_odisha"
AUTH0_DOMAIN = "casa.centroxy.com"

AUTH0_BASE_URL = 'https://' + AUTH0_DOMAIN
AUTH0_AUDIENCE = ""
if AUTH0_AUDIENCE is '':
    AUTH0_AUDIENCE = AUTH0_BASE_URL + '/oxauth/restv1/userinfo'

oauth = OAuth(app)

auth0 = oauth.register(
    'auth0',
    client_id=AUTH0_CLIENT_ID,
    client_secret=AUTH0_CLIENT_SECRET,
    api_base_url=AUTH0_BASE_URL,
    access_token_url=AUTH0_BASE_URL + '/oxauth/restv1/token',
    authorize_url=AUTH0_BASE_URL + '/oxauth/restv1/authorize',
    client_kwargs={
        'scope': 'openid profile',
    },
)

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if constants.PROFILE_KEY not in session:
            return redirect('/gluulogin')
        return f(*args, **kwargs)

    return decorated

@app.route('/')
def home():
    return render_template('index.html')

#Callback Urls are mentioned below
@app.route('/gluulogin')
def gluulogin():
    return auth0.authorize_redirect(redirect_uri=AUTH0_CALLBACK_URL, audience=AUTH0_AUDIENCE)

@app.route('/login')
def login():
    return auth0.authorize_redirect(redirect_uri='http://casa.govt.cent/success_sso', audience=AUTH0_AUDIENCE)

@app.route('/login_1')
def login_1():
    return auth0.authorize_redirect(redirect_uri='http://casa.govt.cent/sso_eabhi', audience=AUTH0_AUDIENCE)

@app.route('/login_2')
def login_2():
    return auth0.authorize_redirect(redirect_uri='http://casa.govt.cent/sso_rccsm', audience=AUTH0_AUDIENCE)


#Template Call
@app.route('/edis')
def e_dispatch():
    if 'server' in session:
        server = session['server']
        print(server)
        return redirect(url_for("login"))
    else:
        return render_template('e_dispatch.html')

@app.route('/eabhi')
def e_abhijog():
    if 'server' in session:
        server = session['server']
        print(server)
        return redirect(url_for("login_1"))
    else:
        return render_template('e_abhijog.html')

@app.route('/rccms')
def rccms():
    if 'server' in session:
        server = session['server']
        print(server)
        return redirect(url_for("login_2"))
    else:
        return render_template('rccms.html')

#After Authenticated by Gluu

@app.route('/callback')
def callback_handling():
    auth0.authorize_access_token(verify=False)
    resp = auth0.get('oxauth/restv1/userinfo', verify=False)
    userinfo = resp.json()
    session[constants.JWT_PAYLOAD] = userinfo
    session[constants.PROFILE_KEY] = {
        'user_id': userinfo['sub'],
        'name': userinfo['name'],

    }
    return render_template('index.html')

@app.route('/dash')
@requires_auth
def dash():
    return render_template('gludata.html',userinfo=session[constants.PROFILE_KEY], userinfo_pretty=json.dumps(session[constants.JWT_PAYLOAD], indent=4))

@app.route('/success_sso')
def success():
    name = 'Gluuserver'
    session['server'] = name
    auth0.authorize_access_token(verify=False)
    resp = auth0.get('oxauth/restv1/userinfo', verify=False)
    userinfo = resp.json()
    session[constants.JWT_PAYLOAD] = userinfo
    session[constants.PROFILE_KEY] = {
        'user_id': userinfo['sub'],
        'name': userinfo['name'],

    }
    return render_template('e_dispatch.html', userinfo=session[constants.PROFILE_KEY],
                           userinfo_pretty=json.dumps(session[constants.JWT_PAYLOAD], indent=4))

@app.route('/sso_eabhi')
def success_eabhi():
    name = 'Gluuserver'
    session['server'] = name
    auth0.authorize_access_token(verify=False)
    resp = auth0.get('oxauth/restv1/userinfo', verify=False)
    userinfo = resp.json()
    session[constants.JWT_PAYLOAD] = userinfo
    session[constants.PROFILE_KEY] = {
        'user_id': userinfo['sub'],
        'name': userinfo['name'],

    }
    return render_template('e_abhijog.html', userinfo=session[constants.PROFILE_KEY],
                           userinfo_pretty=json.dumps(session[constants.JWT_PAYLOAD], indent=4))

@app.route('/sso_rccsm')
def sso_rccms():
    name = 'Gluuserver'
    session['server'] = name
    auth0.authorize_access_token(verify=False)
    resp = auth0.get('oxauth/restv1/userinfo', verify=False)
    userinfo = resp.json()
    session[constants.JWT_PAYLOAD] = userinfo
    session[constants.PROFILE_KEY] = {
        'user_id': userinfo['sub'],
        'name': userinfo['name'],

    }
    return render_template('rccms.html', userinfo=session[constants.PROFILE_KEY],
                           userinfo_pretty=json.dumps(session[constants.JWT_PAYLOAD], indent=4))


#Logout Urls are here
@app.route('/logout')
def gluulogout():
    session.clear()
    params = {'post_logout_redirect_uri': "http://casa.govt.cent/edis",
              'client_id': AUTH0_CLIENT_ID}
    print(auth0.api_base_url)
    return redirect(auth0.api_base_url + '/oxauth/restv1/end_session?' + urlencode(params))

@app.route('/logout_1')
def gluulogout_1():
    session.clear()
    params = {'post_logout_redirect_uri': "http://casa.govt.cent/eabhi",
              'client_id': AUTH0_CLIENT_ID}
    print(auth0.api_base_url)
    return redirect(auth0.api_base_url + '/oxauth/restv1/end_session?' + urlencode(params))

@app.route('/logout_2')
def gluulogout_2():
    session.clear()
    params = {'post_logout_redirect_uri': "http://casa.govt.cent/rccms",
              'client_id': AUTH0_CLIENT_ID}
    print(auth0.api_base_url)
    return redirect(auth0.api_base_url + '/oxauth/restv1/end_session?' + urlencode(params))



if __name__ == '__main__':
    app.run(debug=True, host='127.0.0.1', port='80')